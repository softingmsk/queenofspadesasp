﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace QueenSpadesMVC.Controllers
{
    public class GamesController : Controller
    {
        
      public ActionResult QueenOfSpades()
        {
            return View();
        }

        public ActionResult QueenOfSpadesResult ()
        {
            return View();
        }

        public ActionResult QueenOfSpadesShow()
        {
            return View();
        }

        public ActionResult ZarOfHill()
        {
            return View();
        }
        public ActionResult ZarOfHillGame()
        {
            return View();
        }
    }
}
